/* eslint-disable @next/next/no-img-element */
import { Button, Col, Nav } from "react-bootstrap";
import { Link } from "react-bootstrap-icons";


export default function ProjectCards({title, description, image, url}:any){
    


    return(
        <>
        <Col size={12} sm={6} md={4}>
        <Nav.Link href={url}>
        <div className="proj-imgbx">
            <img src={image} alt="imgCard" />
            <div className="proj-txtx">
                <h5>{title}</h5>
                <span>{description}</span>
            </div>
        </div>
            </Nav.Link>
        </Col>
        </>
    )
}
