/* eslint-disable @next/next/no-img-element */
import { useRouter } from 'next/router';
import { Col, Container, Row } from 'react-bootstrap';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';


export default function Skills(){
    const router = useRouter();
    const { locale } = router;
    
    const responsive = {
        superLargeDesktop: {
          
          breakpoint: { max: 4000, min: 3000 },
          items: 5
        },
        desktop: {
          breakpoint: { max: 3000, min: 1024 },
          items: 3
        },
        tablet: {
          breakpoint: { max: 1024, min: 464 },
          items: 2
        },
        mobile: {
          breakpoint: { max: 464, min: 0 },
          items: 1
        }
      };
    return(
        <section className="skill" id='skills'>
            <Container>
                <Row>
                    <Col>
                    <div className="skill-bx">
                        <h2>
                        {locale== 'fr'? 'Compétences' : 'Skills'}
                        </h2>
                        <Carousel responsive={responsive} infinite={true} className="skill-slider">
                                <div className="item">
                                    <img src="/img/htmlicon.png" alt="skill-img" />
                                    {/* <h5>HTML5</h5> */}
                                </div>
                                <div className="item">
                                    <img src="/img/cssicon.png" alt="skill-img" />
                                    {/* <h5>CSS3</h5> */}
                                </div>
                                <div className="item">
                                    <img src="/img/tsicon.png" alt="skill-img" />
                                    {/* <h5>Typescript</h5> */}
                                </div>
                                <div className="item">
                                    <img src="/img/phpicon.png" alt="skill-img" />
                                    {/* <h5>PHP</h5> */}
                                </div>
                                <div className="item">
                                    <img src="/img/mysqlicon.png" alt="skill-img" />
                                    {/* <h5>MySql</h5> */}
                                </div>
                                <div className="item">
                                    <img src="/img/reacticon.png" alt="skill-img" />
                                    {/* <h5>React</h5> */}
                                </div>
                                <div className="item">
                                    <img src="/img/logosymfony.png" alt="skill-img" />
                                    {/* <h5>Symfony</h5> */}
                                </div>
                                <div className="item">
                                    <img src="/img/logodoctrine.png" alt="skill-img" />
                                    {/* <h5>Doctrine</h5> */}
                                </div>
                                <div className="item">
                                    <img src="/img/logotailwind.png" alt="skill-img" />
                                    {/* <h5>Tailwind</h5> */}
                                </div>
                                <div className="item">
                                    <img src="/img/logoframermotion.webp" alt="skill-img" />
                                    {/* <h5>Framermotion</h5> */}
                                </div>
                        </Carousel>

                    </div>
                    </Col>
                </Row>
            </Container>
            <img className="background-image-left" src="/img/color-sharp.png" alt="Image" />
        </section>
    )
}