/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */
import { setMaxIdleHTTPParsers } from "http";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { ArrowRightCircle, YinYang } from "react-bootstrap-icons";
import { idText } from "typescript";

export default function Banner() {
    const [loopNum, setLoopNum] = useState(0);
    const [isDeleting, setisDeleting] = useState(false);
    const [text, setText] = useState('');
    const [delta, setDelta] = useState(200 - Math.random() * 100);
    const toRotate = ['Hi there! I\'m Yann. Junior Web Developer.'];
    const period = 2000;

    const router = useRouter();
    const {locale} = router;

    useEffect(() => {
        let ticker = setInterval(() => {
            tick();
        }, delta)

        return () => { clearInterval(ticker) };
    }, [text])

    const tick = () => {
        let i = loopNum % toRotate.length;
        let fullText = toRotate[i];
        let updatedText = isDeleting ? fullText.substring(0, text.length - 1) : fullText.substring(0, text.length + 1);

        setText(updatedText);
    }


    return (
        <section className="banner" id="home">
            <Container>
                <Row>
                    <Col xs={12} md={6} xl={7}>
                        <span className="tagline">{locale== 'fr'? 'Bienvenue sur mon portfolio' : 'Welcome to my portfolio'}</span>
                        <h1><span className='wrap'>{text}</span></h1>
                        <p>{locale== 'fr'? 'Après avoir exploré différent recoins de la planète, j\'ai trouvé ma voie dans le développement informatique. Je suis actuellement en formation Développement Web et Mobile à Simplon, je souhaite poursuivre en Conception et Développement d\'Applications. Je suis donc activement à la recherche d&aposune entreprise pour une alternance de 18 mois à compter de Septembre. ' : 'After a while exploring different places of this stunning world, I found my way in programming and developping. Currently in training at Simplon in Lyon, I am learning web and mobile development. In order to keep learning and improve my skills, I am actively looking for an 18 months apprenticeship from 18/09/23 to 21/03.25 !'}
                            
                            
                         
                        </p>
                        <button><a href="./img/yannpezavantcv.pdf">
                            {locale== 'fr'? 'Voir CV' : 'Download CV'}<ArrowRightCircle size={20} /></a></button>
                    </Col>
                    <Col xs={12} md={6} xl={5}>
                        <img src="/img/astronaut-removebg.png" alt="header-img" />
                    </Col>
                </Row>
            </Container>
        </section>
    )
}