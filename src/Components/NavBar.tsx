/* eslint-disable @next/next/no-img-element */
import { StaticImageData } from "next/image";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { Container, Nav, Navbar} from "react-bootstrap";
import { Link } from "react-bootstrap-icons";
import LanguageSelectDropDown from "./LanguageSelectDropDown";




export default function NavBar(){ 
    const [activeLink, setActiveLink]=useState('home');
    const [scrolled, setScrolled]=useState(false);
    const router = useRouter();
    const { locale } = router;


    useEffect(()=>{
        const onScroll=()=>{
            if(window.scrollY>50){
                setScrolled(true);
            }else{
                setScrolled(false); 
            }
        }
        window.addEventListener("scroll", onScroll);

        return () => window.removeEventListener("scroll", onScroll);
    },[])

    const onUpdateActiveLink = (value:any) =>{
        setActiveLink(value);
    }

    return (
        <>
            <Navbar  expand="lg" className={scrolled? "scrolled" : ""}>
                <Container>
                    <Navbar.Brand href="#home">
                        <img className="navbar-logo" src="/img/yannlogonobg.png" alt="Logo"/>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav">
                        <span className="navbar-toggler-icon"></span>
                    </Navbar.Toggle>
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link href="#home" className={activeLink === 'home'?'active navbar-link':'navbar-link'} 
                            onClick={()=>onUpdateActiveLink('home')}>{locale== 'fr'? 'Accueil' : 'Home'}
                                </Nav.Link>
                            <Nav.Link href="#skills" className={activeLink === 'skills'?'active navbar-link':'navbar-link'}
                            onClick={()=>onUpdateActiveLink('skills')}>{locale== 'fr'? 'Compétences' : 'Skills'}
                                </Nav.Link>
                            <Nav.Link href="#project" className={activeLink === 'projects'?'active navbar-link':'navbar-link'}onClick={()=>onUpdateActiveLink('projects')}>{locale== 'fr'? 'Projets' : 'Projects'}
                                </Nav.Link>
                        </Nav>
                        <span className="navbar-text">
                            
                                
                                <Nav.Link href="https://www.linkedin.com/in/yannpezavant/"><img src="/img/linkedinicon.png" alt="linkedin"/> </Nav.Link>
                                <Nav.Link href="https://gitlab.com/yannpezavant"><img src="/img/gitlabicon2.png" alt="gitlab"/> </Nav.Link>
                                <Nav.Link href="https://api.whatsapp.com/send?phone=33686337184"><img src="/img/whatsapp.png" alt="whatsapp"/></Nav.Link>
                                <Nav.Link href="#connect"><img src="/img/mailicon.png" alt="whatsapp"/></Nav.Link>
                            
                            

                                {/* <Nav.Link href="#connect" className={activeLink === 'contact'?'active navbar-link':'navbar-link'}onClick={()=>onUpdateActiveLink('contact')}>Contact</Nav.Link> */}
                            
                        </span>
                        
                        <div>
                        <LanguageSelectDropDown />
                        </div>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>

    )


}
    