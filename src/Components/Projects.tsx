/* eslint-disable @next/next/no-img-element */
import image from "next/dist/client/image";
import { title } from "process";
import { Col, Container, Nav, Row, Tab } from "react-bootstrap";
import { Link } from "react-bootstrap-icons";
import ProjectCard from "./ProjectCard";
import { useRouter } from "next/router";


export default function Projects(){
    const router = useRouter();
    const { locale } = router;
    
    const projectsTabOne= [
        {
            
            title: "My first portfolio",
            description:"Developed with HTML et CSS",
            image: ('/img/portfolio.png'),
            url:'https://portfolio-yannpezavant.netlify.app/'
            
            
        },
        {
            title: "Quiz",
            description:"Developed with HTML, CSS",
            image: ('/img/quiz2.png'),
            url:'https://projet-quiz.netlify.app/'
        },
        {
            title: "Brick breaker",
            description:"Developed with HTML, CSS et Typescript",
            image: ('/img/jsgame.png'),
            url:'https://projet-jeu.netlify.app/'
        }
    ]
    const projectsTabTwo= [
        {
            title: "Back-end of a Community Youth Club",
            description:"Developed with PHP et MySQL",
            image: ('/img/diag-classes-mjc.png'),
            url:'https://gitlab.com/Hisami/mjc-simplon'
        },
        {
            title: "Showcase site of a Coffeeshop",
            description:"Developed with Next.js, Tailwind and FramerMotion",
            image: ('/img/freshcoffeshop.png'),
            url:'https://freshcoffeeshop.netlify.app/'
        },
    ]
    const projectsTabThree= [
        {
            title: "Showcase site of carpenting company",
            description:"Developped with WordPress and Elementor",
            image: ('/img/allairecharpentecouverture.png'),
            url:'https://www.allairecharpentecouverture.fr/'
        }
    ]
    return(
        <>
        <section className="project" id="project">
            <Container>
                <Row>
                    <Col>
                    <h2>
                    {locale== 'fr'? 'Projets' : 'Projects'}</h2>
                    <p></p>
                    <Tab.Container id="projects-tabs" defaultActiveKey="first">
                        <Nav variant="pills" defaultActiveKey="/home" className="nav-pills mb-5 justify-content-center align-items-center" id="pills-tab">
                                <Nav.Item>
                                    <Nav.Link eventKey="first">{locale== 'fr'? 'Partie 1' : 'Part 1'}</Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link eventKey="second">{locale== 'fr'? 'Partie 2' : 'Partie 2'}</Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link eventKey="third">{locale== 'fr'? 'WordPress' : 'WordPress'}</Nav.Link>
                                </Nav.Item>
                            </Nav>
                            <Tab.Content>
                                <Tab.Pane eventKey="first">
                                    <Row>
                                        {
                                            projectsTabOne.map((project, index)=>{
                                                return(
                                                    <ProjectCard
                                                        key={index}
                                                        {...project}                                                    
                                                        />
                                                )
                                            })
                                        }
                                    </Row>
                                </Tab.Pane>
                                <Tab.Pane eventKey="second">
                                <Row>
                                        {
                                            projectsTabTwo.map((project, index)=>{
                                                return(
                                                    <ProjectCard
                                                        key={index}
                                                        {...project}                                                    
                                                        />
                                                )
                                            })
                                        }
                                    </Row>

                                </Tab.Pane>
                                <Tab.Pane eventKey="third">
                                <Row>
                                        {
                                            projectsTabThree.map((project, index)=>{
                                                return(
                                                    <ProjectCard
                                                        key={index}
                                                        {...project}                                                    
                                                        />
                                                )
                                            })
                                        }
                                    </Row>
                                    </Tab.Pane>
                            </Tab.Content>
                    </Tab.Container>
                    </Col>
                </Row>
            </Container>
            <img className="background-image-right" src="/img/color-sharp2.png" alt="Image"></img>
        </section>
        
        </>
    )
}