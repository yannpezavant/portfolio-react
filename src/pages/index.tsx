import NavBar from "@/Components/NavBar";
import Banner from "@/Components/Banner";
import Skills from "@/Components/Skills";
import React from "react";
import Projects from "@/Components/Projects";
import Contact from "@/Components/Contact";




export default function Index(){
  return(
    <>
    <NavBar/>
    <Banner/>
    <Skills/>
    <Projects/>
    <Contact/>
    </>
  )
}