const express = require('express');
const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');

const app = express();
app.use(bodyParser.json());

app.post('/contact', async (req, res) => {
  const { firstName, lastName, phone, email, message } = req.body;

  const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    auth: {
      user: 'your-gmail-username',
      pass: 'your-gmail-password',
    },
  });

  try {
    const info = await transporter.sendMail({
      from: `${firstName} ${lastName} <${email}>`,
      to: 'your-email-address',
      subject: 'New message from your website',
      text: `Name: ${firstName} ${lastName}\nPhone: ${phone}\nEmail: ${email}\nMessage: ${message}`,
    });
    console.log(info);
    res.sendStatus(200);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

const port = process.env.PORT || 5000;
app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
